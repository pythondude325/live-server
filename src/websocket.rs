
use warp::filters::ws;
// use warp::Filter;

use futures::sync::mpsc;
use futures::{Future, Sink, Stream};

use std::collections::HashMap;
use std::sync::{Arc, Mutex};

pub type SocketMap = Arc<Mutex<HashMap<String, mpsc::UnboundedSender<ws::Message>>>>;

pub fn new_socketmap() -> SocketMap {
    Arc::new(Mutex::new(HashMap::new()))
}

pub fn websocket_connected(
    ws: ws::WebSocket,
    path: String,
    socket_map: SocketMap,
) -> impl Future<Item = (), Error = ()> {
    println!("Upgraded websocket");

    let (ws_tx, ws_rx) = ws.split();
    let (tx, rx) = mpsc::unbounded();

    warp::spawn(rx.forward(ws_tx.sink_map_err(|_| ())).map(|_tx_rx| ()));

    socket_map.lock().unwrap().insert(path.clone(), tx);

    let socket_map_2 = socket_map.clone();
    let disconnect_path = path.clone();

    ws_rx
        .for_each(|_| Ok(()))
        .then(move |result| {
            println!("{} disconnected from websocket", disconnect_path);

            socket_map_2.lock().unwrap().remove(&disconnect_path);

            result
        })
        .map_err(|e| {
            eprintln!("websocket error: {}", e);
        })
}

